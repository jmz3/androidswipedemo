package com.jacintomendes.swipedemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.jacintomendes.swipedemo.fragments.Android;
import com.jacintomendes.swipedemo.fragments.Ios;
import com.jacintomendes.swipedemo.fragments.Windows;

/**
 * Created by Jm on 20-Sep-14.
 */
public class TabPagerAdapter extends FragmentStatePagerAdapter {

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch(i) {
            case 0:
                return new Android();
            case 1:
                return new Ios();
            case 2:
                return new Windows();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
