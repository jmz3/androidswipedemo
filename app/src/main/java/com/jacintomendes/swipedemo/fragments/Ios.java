package com.jacintomendes.swipedemo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jacintomendes.swipedemo.R;

/**
 * Created by Jm on 20-Sep-14.
 */
public class Ios extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View ios = inflater.inflate(R.layout.ios_frag, container, false);
        return ios;
    }
}
